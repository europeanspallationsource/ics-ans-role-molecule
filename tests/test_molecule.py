import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('all')


def test_default(Command):
    cmd = Command('/opt/conda/envs/molecule/bin/molecule --help 2>&1')
    assert cmd.rc == 0
    assert 'Usage: molecule [OPTIONS] COMMAND [ARGS]...' in cmd.stdout
