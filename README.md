[DEPRECATED] ics-ans-role-molecule
==================================

**WARNING! This role is deprecated!**

The role was only creating a molecule conda environment.
The new conda role can do that by passing environment files via the `conda_env_files` variable.

----

Ansible role to install molecule.

Requirements
------------

- ansible >= 2.2
- molecule >= 1.20

Role Variables
--------------

None

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-molecule
```

License
-------

BSD 2-clause
